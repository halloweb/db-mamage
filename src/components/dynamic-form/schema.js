export default {
  props: [
    {
      code: 'layout',
      default: 'flex',
      rangeset: [
        {
          value: 'flex'
        },
        {
          value: 'float'
        },
        {
          value: 'table'
        }
      ]
    },
    {
      code: 'columns',
      default: 2,
      type: 'number'
    },
    {
      code: 'view',
      type: 'bool',
      default: false
    },
    {
      code: 'labelAlign',
      rangeset: [
        {
          value: 'left'
        },
        {
          value: 'center'
        },
        {
          value: 'right'
        }
      ]
    },
    {
      code: 'labelWrap',
      type: 'bool',
      default: false
    },
    {
      code: 'validatorType',
      rangeset: [
        {
          value: 'default'
        },
        {
          value: 'poptip'
        },
        {
          value: 'none'
        }
      ]
    },
    {
      code: 'disabledValidator'
    }
  ]
}
