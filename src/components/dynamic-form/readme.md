## dynamic-form

 属性 | 说明 | 类型 | 默认值
 --- | ---  | ---  | --- 
 v-model       | 双向绑定表单值                     | object
 model         | [example](#modelexample)                               | array
 label-width   | 字段名label容器的宽度               | [Number,String] |
 labelPosition | label位置 ['left', 'right', 'top'] | String          | 'right' 
 inline        | 是否开启行内表单模式,可选值['': 自由排列,'2':2列, '3':3列] | string | ''
方法 | 说明 | 返回值
 --- | ---  | ---
fieldChange | model和数据只有一层时起作用type为arrStruct、objStruct下值改变时无用最好watch表单总数据然后操作| {code,value,oldValue}
validate | 表单校验 | {valid: true或false ,message: ,data: }
reset | 重置表单
```javascript
this.$refs.apiForm.validate().then(res => {
        if(res.valid) {
          // 校验通过
        }
}
```
### 控件类型
> 控件配置描述等详细信息参考下面[model example](#modelexample)
```javascript
{string, number, text, percent, select, selectMulti, radioGroup, checkbox, checkList, switch, date, time, selectStep, codeEditor, color, list, stringList}
```
### modelexample
```javascript
    {fields :[
          {
            code: "code", // 字段编码
            name: "编码", // 字段名称
            type: "string", // 使用的控件类型
            default: "", // 默认值
            span: 2, // 当表单为inline模式时当前字段控件占用的列数
            rules:[  // 校验规则 type(range\phone\email) pattern(正则表达式\正则表达式 字符串)
              {required: true, message: '请输入编码', trigger: 'blur'},
              {type: 'range', max: 10, min: 5, message: '编码长度应该大于5小于10'},
              {type: 'phone',  message: '请输入正确电话号码',},
              {pattern: '/^1[3456789]\\d{9}$/',  message: '请输入正确电话号sds码s'}
              ],
            attrs:{ // iview中存在的控件属性参考iview对应控件属性设置
              placeholder: '请输入编码', disabled: true}
           },
          {
            code: "amount",
            name: "金额",
            type: "number",
            rules:[
              {required: true, message: '请输入金额', trigger: 'blur'}
            ]
          },
            { // arrStruct 字段为对象数组[{},{}] 
            // 1.当数组中每一项对象字段相同时children为对象类型只需要一个fields描述即可，当数组的值为string或number时fields中只包含一个控件的描述并且不要code值['1','2']
            // 2.当每一项对象包含字段不同时children为数组类型数组的每一项都有一个fields各自描述自己的字段组成[{fields:[]},{fields:[]}]
            code: "arrStruct",
            name: "arr分组",
            type: "arrStruct",
            attrs: {labelPosition: 'right', labelWidth:'80'},
            children: {
              fields: [
              {
                code: "group1",
                name: "group1",
                type: "string",
                attrs:{placeholder: '请输入group1'}
              },
              {
                code: "group2",
                name: "group22",
                type: "number",
                rules:[
                  {required: true, message: '请输入group2', trigger: 'blur'}
                ]
              }
              ]
            } 
          },
          { // objStruct或form 字段为对象{a:'',b:''}
            code: "objStruct",
            name: "obj分组",
            type: "objStruct", // objStruct 或 form
            attrs: {labelPosition: 'right',labelWidth:'80'},
            children: {
              fields: [
              {
                code: "group1",
                name: "group1",
                type: "string",
                attrs:{placeholder: '请输入group1'}
              },
              {
                code: "group2",
                name: "group22",
                type: "number",
                rules:[
                  {required: true, message: '请输入group2', trigger: 'blur'}
                ]
              }
              ]
            } 
          },
          {
            code: "progress",
            name: "进度",
            type: "percent",
            rules:[
              {required: true, message: '请选择进度'}
            ]
          },
          {
            code: "area",
            name: "地区",
            type: "select",
            attrs: {placeholder: '请选择地区'},
            dataList: [
              {value:'北京',name: '北京'},
              {value:'南京',name: '南京'}
            ],
             rules:[
              {required: true, message: '请选择地区', trigger: 'change'}
            ]
          },
          {
            code: "des",
            name: "说明",
            type: "text",
            attrs: {placeholder: '请输入说明'},
            rules:[
              {required: true, message: '请输入说明', trigger: 'blur'}
            ]
          },
          {
            code: "citys",
            name: "城市",
            type: "selectMulti",
            required: true,
            dataList: [
              {value:'北京',name: '北京'},
              {value:'南京',name: '南京'}
            ],
             rules:[
              {required: true, message: '请选择城市', trigger: 'change'}
            ]
          },
          {
            code: "good",
            name: "宜居",
            type: "radioGroup",
            dataList: [
              {value: 1,name: '是'},
              {value: 2,name: '否'}
            ],
            attrs: {type: 'button'},
            rules:[
              {required: true, message: '请选择'}
            ]
          },
          {
            code: "accept",
            name: "接受",
            type: "checkbox"
          },
          {
            code: "applyType",
            name: "申请类型",
            type: "checkList",
            dataList: [{
              name: '疾病',
              value: '疾病'
            },
            {
              name: '意外',
              value: '意外'
            }
            ]
          },
          {
            code: 'isOpen',
            name: '是否打开',
            type: 'switch'
          },
          {
            code: 'date',
            name: '日期',
            type: 'date',
            attrs: {placeholder: '请'},
          },
          {
            code: 'time',
            name: '时间',
            type: 'time',
            attrs: {type: 'timerange',placeholder: '请'},
          },
          { code: 'selectSteps',
            name: '联动',
            type: 'selectStep',
            attrs: {placeholder: '请'},
            dataList: [{
                    value: 'beijing',
                    label: '北京',
                    children: [
                        {
                            value: 'gugong',
                            label: '故宫'
                        },
                        {
                            value: 'tiantan',
                            label: '天坛'
                        },
                        {
                            value: 'wangfujing',
                            label: '王府井'
                        }
                    ]
                }]
          },{
            code: 'objCode',
            name: '对象',
            type: 'codeEditor',
             rules:[
              /**
               * 自定义的validator包含参数
               * formData:此表单所有字段数据、vm:当前control组件实例、value:当前字段值、
               * rule:本条校验规则对象、callback:回调函数
               * callback(new Error('错误信息'))
               */
              { validator: ({formData,vm,value,rule,callback}) => {
              vm.onError ? callback(new Error('json格式不正确')) : callback()}}
            ]
          },
          {
            code: "url",
            name: "数据源地址",
            type: "stringList",
            splitter: ",", // 分割符默认逗号
            attrs: { placeholder: '请输入数据源地址多个以逗号隔开' }
          },
          {
            code: "keyValue",
            name: "键值对",
            type: "keyValue"
          },
           {
            code: "list",
            name: "列表",
            span: 2,
            type: "list",
            height: 200, // 列表高度超出产生滚动条默认300
            fields: [  // 当fields中字段disabled不为true时渲染控件反之直接展示数据
                {
                  code: "name",
                  name: "name",
                  type: "string",
                },
                {
                  code: "accept",
                  name: "接受",
                  type: "checkbox"
                },
                 {
                  code: 'date',
                  name: '日期',
                  type: 'date',
                  attrs: { placeholder: '请' },
                },
                {
                  code: "progress",
                  name: "进度",
                  type: "percent"
                },
              ]
          },
        ]
    }
```