import axios from 'axios'

export function link(_url, data, _option) {
  // let baseURL = process.env.VUE_APP_API
  let newData = data
  let option = _option || {}
  let url = _url
  // if (
  //   url &&
  //   url.length &&
  //   url[url.length - 1] !== '&' &&
  //   url[url.length - 1] !== '?'
  // ) {
  //   url += '&';
  // }
  // if (option.method === 'put' && data) {
  //   newData.resource[0].update_time = new Date();
  // }
  // url += 'api_key=' + apiKey;
  url = url.replace(/\+/g, '%2B')
  // url = unescape(encodeURIComponent(url));
  return axios({
    url,
    method: option.method || 'GET',
    data: newData,
    headers: {
      'X-Requested-With': 'XMLHttpRequest'
      // 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    }
  }).then(res => {
    if (option.sourceData) {
      return res
    } else {
      return res.data
    }
  })
}
