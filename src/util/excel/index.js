import XLSX from 'xlsx'
export function readExcel(file) {
  return new Promise(resolve => {
    const reader = new FileReader()
    reader.readAsArrayBuffer(file)
    reader.onload = function(e) {
      const data = e.target.result
      let wb = XLSX.read(data, {
        type: 'array'
      })
      let dataObj = wb.Sheets[wb.SheetNames[0]]
      console.log(dataObj)
      let firstRow = [
        dataObj['A1'] ? dataObj['A1'].w || dataObj['A1'].v : '',
        dataObj['B1'] ? dataObj['B1'].w || dataObj['B1'].v : '',
        dataObj['C1'] ? dataObj['C1'].w || dataObj['C1'].v : '',
        dataObj['D1'] ? dataObj['D1'].w || dataObj['D1'].v : '',
        dataObj['E1'] ? dataObj['E1'].w || dataObj['E1'].v : '',
        dataObj['F1'] ? dataObj['F1'].w || dataObj['F1'].v : ''
      ]
      let rowCount =
        dataObj['!ref'].slice(dataObj['!ref'].indexOf('A1:IS') + 5) - 0
      let head = [
        dataObj['A2'].w || dataObj['A2'].v,
        dataObj['B2'].w || dataObj['B2'].v,
        dataObj['C2'].w || dataObj['C2'].v,
        dataObj['D2'].w || dataObj['D2'].v,
        dataObj['E2'].w || dataObj['E2'].v,
        dataObj['F2'].w || dataObj['F2'].v
      ]
      let dataList = []
      for (let i = 3; i <= rowCount; i++) {
        let row = {
          origin_field: dataObj['A' + i]
            ? dataObj['A' + i].w || dataObj['A' + i].v
            : '',
          new_field: dataObj['B' + i]
            ? dataObj['B' + i].w || dataObj['B' + i].v
            : '',
          dataType: dataObj['D' + i]
            ? dataObj['D' + i].w || dataObj['D' + i].v
            : '',
          methods: dataObj['E' + i]
            ? dataObj['E' + i].w || dataObj['E' + i].v
            : '',
          remark: dataObj['F' + i]
            ? dataObj['F' + i].w || dataObj['F' + i].v
            : '',
          fieldMean: ''
        }
        dataList.push(row)
      }
      console.log({ dataList, head, firstRow })
      resolve({ dataList, head, firstRow })
    }
  })
}
