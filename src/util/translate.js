import md5 from 'js-md5'
import Vue from 'vue'
const appid = '20191210000364857'
const key = '7CJLK16aNpZygFGk8vpf'
export function translate(query, from = 'zh', to = 'en') {
  console.log(query)
  let salt = new Date().getTime()
  // 多个query可以用\n连接  如 query='apple\norange\nbanana\npear'
  let str1 = appid + query + salt + key
  let sign = md5(str1)
  return Vue.prototype.$jsonp(
    'http://api.fanyi.baidu.com/api/trans/vip/translate',
    {
      q: query,
      appid: appid,
      salt: salt,
      from: from,
      to: to,
      sign: sign
    }
  )
}
