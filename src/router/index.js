import Vue from 'vue'
import VueRouter from 'vue-router'
import ApiDev from '@/view/apidev'
import ETL from '@/view/etl'
import Profiling from '@/view/profiling'
Vue.use(VueRouter)
const routes = [
  { path: '/', redirect: '/dataeditor' },
  { path: '/dataeditor', component: ApiDev },
  { path: '/etl', name: 'etl', component: ETL },
  { path: '/profiling', name: 'profiling', component: Profiling }
]
const router = new VueRouter({ routes })
export default router
