import Vue from 'vue'
import App from './App.vue'
import ViewUI from 'view-design'
import router from '@/router'
import 'view-design/dist/styles/iview.css'
import VueJsonp from 'vue-jsonp'
Vue.use(VueJsonp)

Vue.use(ViewUI)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
